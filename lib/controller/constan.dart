import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:excel/excel.dart';
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';

class Api {
  static Future<void> hitapi(String bareer, String api, String tipemethod,
      List<Map<String, dynamic>> dataList) async {
    var dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer $bareer';
    List<Map<String, dynamic>> respon = [];
    Logger().i(tipemethod);
    Logger().w(dataList);
    for (var i = 0; i < dataList.length; i++) {
      try {
        String modifiedUrl = buildUrl(api, dataList[i]);
        Logger().w(bareer);
        Logger().e(dataList[i]);
        Logger().i(modifiedUrl);
        Response response = tipemethod == "GET"
            ? await dio.get(
                modifiedUrl,
                options: Options(
                  headers: {
                    'Content-Type': 'application/json',
                  },
                ),
              )
            : await dio.post(
                api.toString(),
                data: dataList[i],
                options: Options(
                  headers: {
                    'Content-Type': 'application/json',
                  },
                ),
              );

        Logger().w(response.statusMessage);
        Map<String, dynamic> data = {};
        Logger().w(response.statusMessage, response.statusCode.toString());
        String key =
            '${response.statusCode.toString()} $modifiedUrl'; // Assuming you want to use the text of the controller as the key
        String value = jsonEncode(response.data);
        data[key] = value;
        respon.add(data);
      } catch (e) {
        Logger().w(e);
        print(e);
      }
    }

    await Future.delayed(const Duration(seconds: 1), () async {
      Logger().w(respon);
      await generateErrorExcel(respon, api);
    });
  }

  static String buildUrl(String api, Map<String, dynamic> datamap) {
    Uri uri = Uri.parse(api);

    // Add query parameters to the URI
    uri = uri.replace(queryParameters: datamap);

    // Convert the modified URI back to a string
    return uri.toString();
  }

  static Future<void> generateErrorExcel(
      dynamic responseData, String api) async {
    var excel = Excel.createExcel();
    var sheet = excel['Sheet1'];
    String sanitizedApi = api.replaceAll(RegExp(r'[/:]'), '_');

    // Add headers
    sheet.appendRow(['Status Data', 'API']);
    DateTime now = DateTime.now();

    // Add data
    sheet.appendRow([responseData, api]);
    Directory? downloadsDir = await getDownloadsDirectory();
    String formattedDate =
        "${now.year}-${now.month}-${now.day}_${now.hour}-${now.minute}-${now.second}";

    // Save the Excel file
    final file = '${downloadsDir!.path}/$sanitizedApi/$formattedDate.xlsx';

    final bytes = excel.encode();
    //  final fileis = File('${downloadsDir!.path}/$sanitizedApi.xlsx');

    if (bytes != null) {
      // if (fileis.existsSync()) {
      //   fileis.deleteSync();
      // }
      await Future.delayed(const Duration(seconds: 1));
      await File(file).writeAsBytes(bytes);
    } else {
      print('Error generating Excel: Bytes are null.');
    }
  }
}
