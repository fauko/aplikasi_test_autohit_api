import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:postman_gb/index_app.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const MaterialApp(
    debugShowCheckedModeBanner: false,
    debugShowMaterialGrid: false,
    home: IndexApp(),
  ));
}
