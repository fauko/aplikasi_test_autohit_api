import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:excel/excel.dart';
import 'dart:io';
import 'package:logger/logger.dart';
import 'package:csv/csv.dart';
import 'package:postman_gb/controller/constan.dart';

class IndexApp extends StatefulWidget {
  const IndexApp({Key? key}) : super(key: key);

  @override
  State<IndexApp> createState() => _IndexAppState();
}

class _IndexAppState extends State<IndexApp> {
  List<Widget> formFields = [];
  List<TextEditingController> controllers = [];
  List<Widget> formFieldsvalue = [];
  List<TextEditingController> controllersvalue = [];
  TextEditingController apiform = TextEditingController(text: '');
  TextEditingController bareerform = TextEditingController(text: '');
  String selectedMethod = '';

  Map<String, dynamic> mapdata = {};

  List<Map<String, dynamic>> dataList = [];
  File? _selectedFile;

  Future<void> pickAndReadExcel() async {
    final result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: [
        'xlsx',
        'pdf',
        'csv',
        'xls',
      ],
    );

    if (result != null) {
      setState(() {
        _selectedFile = File(result.files.single.path!);
      });
      await readExcel(_selectedFile!.path);
    }
  }

  Future<void> readExcel(String filePath) async {
    var file = Excel.decodeBytes(File(filePath).readAsBytesSync());

    for (var table in file.tables.keys) {
      print(table); //sheet Name
      print(file.tables[table]!.maxColumns);
      print(file.tables[table]!.maxRows);
      for (var row in file.tables[table]!.rows) {
        Map<String, dynamic> data = {};

        for (int i = 0; i < controllers.length; i++) {
          String key = controllers[i]
              .text; // Assuming you want to use the text of the controller as the key
          String value = row[i]?.value.toString() ?? '';
          data[key] = value;
        }

        // Adding the map to the list
        setState(() {
          dataList.add(data);
        });
      }
    }

    // Print the dataList
    print('Data List: $dataList');
  }

  void kuranginform() {
    if (formFields.isNotEmpty) {
      setState(() {
        formFields.removeAt(0);
        controllers.removeAt(0);
      });

      // Cetak hasil List
      print('DataList: ${formFields.length}');
    }
  }

  void kuranginformvalue() {
    if (formFieldsvalue.isNotEmpty) {
      setState(() {
        formFieldsvalue.removeAt(0);
        controllersvalue.removeAt(0);
      });

      // Cetak hasil List
      print('DataList: ${formFields.length}');
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("POSTMANT AUTO by Fauko"),
        backgroundColor: Colors.red,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Tambahkan logika refresh di sini
          setState(() {
            dataList.clear();
            controllers.clear();
            formFields.clear();
            formFieldsvalue.clear();
            controllersvalue.clear();
            apiform.clear();
          });
        },
        tooltip: 'Refresh',
        child: const Icon(Icons.refresh),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.only(left: 10, top: 10, bottom: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Radio(
                    //  title: Text('POST'),
                    value: 'POST',
                    groupValue: selectedMethod,
                    onChanged: (value) {
                      setState(() {
                        selectedMethod = value.toString();
                      });
                    },
                  ),
                  const Text(
                    'POST',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Radio(
                    // title: Text('GET'),
                    value: 'GET',
                    groupValue: selectedMethod,
                    onChanged: (value) {
                      setState(() {
                        selectedMethod = value.toString();
                      });
                    },
                  ),
                  const Text(
                    'GET',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "INPUT BASE URL/API",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              Stack(
                children: [
                  Container(
                    height: 70,
                    width: 400,
                    margin: const EdgeInsets.only(top: 10),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    decoration: BoxDecoration(
                      boxShadow: const [
                        BoxShadow(
                            color: Colors.black26,
                            offset: Offset(0, 1),
                            blurRadius: 6.0)
                      ],
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  Container(
                    width: 400,
                    margin: const EdgeInsets.only(top: 15, bottom: 10),
                    child: TextFormField(
                      controller: apiform,
                      onChanged: (value) {},
                      maxLines: 3,
                      decoration: const InputDecoration(
                        //   prefixIcon: prefixIcon,

                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        hintStyle: TextStyle(fontSize: 13),
                        contentPadding: EdgeInsets.only(left: 20.0),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "INPUT BAREER TOKEN",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              Stack(
                children: [
                  Container(
                    height: 70,
                    width: 400,
                    margin: const EdgeInsets.only(top: 10),
                    padding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    decoration: BoxDecoration(
                      boxShadow: const [
                        BoxShadow(
                            color: Colors.black26,
                            offset: Offset(0, 1),
                            blurRadius: 6.0)
                      ],
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  Container(
                    width: 400,
                    margin: const EdgeInsets.only(top: 15, bottom: 10),
                    child: TextFormField(
                      controller: bareerform,
                      onChanged: (value) {},
                      maxLines: 3,
                      decoration: const InputDecoration(
                        //   prefixIcon: prefixIcon,

                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        hintStyle: TextStyle(fontSize: 13),
                        contentPadding: EdgeInsets.only(left: 20.0),
                      ),
                    ),
                  ),
                ],
              ),

              Container(
                width: 450,
                height: 1,
                margin: const EdgeInsets.only(top: 20, bottom: 20),
                color: Colors.black,
              ),
              // parameter
              const Text(
                "INPUT KEY PARAMETER",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: formFields.length,
                itemBuilder: (context, index) {
                  return formFields[index];
                },
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        TextEditingController controller =
                            TextEditingController();
                        controllers.add(controller);
                        formFields.add(buildTextField(controller));
                      });
                    },
                    child: const Text('Tambah Field'),
                  ),
                  const SizedBox(
                    width: 30,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      kuranginform();
                    },
                    child: const Text('Kurangin Field'),
                  ),
                ],
              ),
              const SizedBox(
                height: 30,
              ),

              Container(
                width: 450,
                height: 1,
                margin: const EdgeInsets.only(top: 20, bottom: 20),
                color: Colors.black,
              ),
              // value
              const Text(
                "INPUT VALUE PARAMETER",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: formFieldsvalue.length,
                itemBuilder: (context, index) {
                  return formFieldsvalue[index];
                },
              ),
              const SizedBox(
                height: 20,
              ),
              if (_selectedFile != null) ...[
                Text('Selected File: ${_selectedFile!.path}'),
              ],
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ElevatedButton(
                    onPressed: formFields.isNotEmpty ? pickAndReadExcel : null,
                    child: const Text('Tambah File picker'),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  const Text("OR"),
                  const SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      if (formFields.isNotEmpty) {
                        setState(() {
                          TextEditingController controller =
                              TextEditingController();
                          controllersvalue.add(controller);
                          formFieldsvalue.add(buildTextFieldValue(controller));
                        });
                      } else {
                        Logger().w(2 > 2);
                      }
                    },
                    child: const Text('Tambah Form'),
                  ),
                  const SizedBox(
                    width: 30,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      kuranginformvalue();
                    },
                    child: const Text('Kurangin Field'),
                  ),
                ],
              ),
              const SizedBox(
                height: 30,
              ),

              Container(
                width: 450,
                height: 1,
                margin: const EdgeInsets.only(top: 20, bottom: 20),
                color: Colors.black,
              ),
              ElevatedButton(
                  onPressed: () {
                    Map<String, dynamic> data = {};
                    if (controllersvalue.isNotEmpty) {
                      for (int i = 0; i < controllers.length; i++) {
                        String key = controllers[i].text;
                        String value = controllersvalue[i].text;

                        // Create a new map for each iteration
                        Map<String, dynamic> newData = Map.from(data);
                        newData[key] = value;
                        // Logger().e(newData);

                        setState(() {
                          // Adding the new map to the list
                          dataList.add(newData);
                        });
                      }
                    }

                    //  dataList.clear();
                    Logger().w(dataList.toList().toSet().length);
                    Future.delayed(const Duration(seconds: 2), () {
                      if (bareerform.text.isNotEmpty &&
                          apiform.text.isNotEmpty &&
                          selectedMethod.isNotEmpty &&
                          dataList.isNotEmpty) {
                        Api.hitapi(
                            bareerform.text.toString(),
                            apiform.text.toString(),
                            selectedMethod.toString(),
                            dataList);
                      }
                    });
                  },
                  child: const Text("SEND DATA"))
            ],
          ),
        ),
      ),
    );
  }

  Widget buildTextField(TextEditingController controller) {
    return Stack(
      children: [
        Container(
          height: 70,
          width: 400,
          margin: const EdgeInsets.only(top: 10),
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          decoration: BoxDecoration(
            boxShadow: const [
              BoxShadow(
                  color: Colors.black26, offset: Offset(0, 1), blurRadius: 6.0)
            ],
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
        Container(
          width: 400,
          margin: const EdgeInsets.only(top: 15, bottom: 10),
          child: TextFormField(
            controller: controller,
            onChanged: (value) {},
            maxLines: 3,
            decoration: const InputDecoration(
              //   prefixIcon: prefixIcon,

              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              hintStyle: TextStyle(fontSize: 13),
              contentPadding: EdgeInsets.only(left: 20.0),
            ),
          ),
        ),
      ],
    );
  }

  Widget buildTextFieldValue(TextEditingController controller) {
    return Stack(
      children: [
        Container(
          height: 70,
          width: 400,
          margin: const EdgeInsets.only(top: 10),
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          decoration: BoxDecoration(
            boxShadow: const [
              BoxShadow(
                  color: Colors.black26, offset: Offset(0, 1), blurRadius: 6.0)
            ],
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
        Container(
          width: 400,
          margin: const EdgeInsets.only(top: 15, bottom: 10),
          child: TextFormField(
            controller: controller,
            onChanged: (value) {},
            maxLines: 3,
            decoration: const InputDecoration(
              //   prefixIcon: prefixIcon,

              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              hintStyle: TextStyle(fontSize: 13),
              contentPadding: EdgeInsets.only(left: 20.0),
            ),
          ),
        ),
      ],
    );
  }
}
